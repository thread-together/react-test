// We are in the browser, so there is no import/require. We can "import/require" by
// destructuring the global React variable.
const { useState, useEffect } = React;

function App() {
  return (
    <h1>Hello, world!</h1>
  );
}
