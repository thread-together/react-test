export default function XIcon() {
    return (
        <svg
            width="100%"
            height="100%"
            viewBox="0 0 16 15"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
        >
            <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(-712.000000, -73.000000)" fill="currentColor">
                    <g transform="translate(589.000000, 60.000000)">
                        <g transform="translate(123.000000, 13.000000)">
                            <path d="M14.2413634,0.748390881 L15.7882795,2.49707857 L9.92314477,7.68439088 L15.7882795,12.8722381 L14.2413634,14.6209258 L8.16214477,9.24239088 L2.0830608,14.6209258 L0.536144769,12.8722381 L6.40014477,7.68439088 L0.536144769,2.49707857 L2.0830608,0.748390881 L8.16214477,6.12639088 L14.2413634,0.748390881 Z"></path>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    );
}
