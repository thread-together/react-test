# On-site Test

## React Exercise

* Display a list of products from the products API.
* When clicking anywhere on the card, show the product details for that item using the product details API.

The design should loosely match the [comp][comp]. The icons are in the public folder.

You are free to use whatever CSS library or framework you are most comfortable with. The focus is on mobile web. You can assume a browser width of 375px. We will be viewing the app on the `iPhone X` device in Chrome.

## Goals

Here is how we are judging this test:

* You are able design a small React application.
* You understand how to interact with server-side APIs.
* You can style web pages using CSS.

### Non-Goals

* Client-side routing. There is no need to update the URL when transitioning between the catalog view and product detail view.
* State containers. The use of a state container (Redux, MobX, Flux, Relay, etc) is overkill for this app.

## APIs

* `GET /api/products` - get a list of all products
* `GET /api/products/:id` - get product details

The express server and API code is in the `src` directory. We bundle the API code to avoid issues with [CORS][CORS] and/or proxies. None of this could should be changed.

## Development

### Building

* Run `npm install`

### Starting server

* Run `node src/index.js`

The server will serve the index.html page and both APIs.

### Source Code

The code to modify is in the `public/` directory. To reduce the change of webpack or babel issues, we are running this React app 100% in the browser. You can put all of your components in `public/App.jsx` or make a separate file for each component. If you make a separate file for each component, please make sure to add them to `public/index.html`.

Because we are in the browser, there are no import/require statements. See our example in `public/App.jsx` on how to access functions.

## Questions?

We realize that time is limited. If you are running into issues, please email recruiting@shopbehold.com and we will try and help.

[comp]: ./comp.jpg
[CORS]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
