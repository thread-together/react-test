const express = require('express');
const bodyParser = require('body-parser');

const db = require('./db');

module.exports = {
  initServer: () => {
    const app = express();
    app.use(bodyParser.json());

    // Serve static assets
    app.use(express.static('public'));

    app.get('/api/products', function(request, response) {
      response.json(db.getAllProducts());
    });

    app.get('/api/products/:id', function(request, response) {
      const { id } = request.params;
      response.json(db.getProduct(Number(id)));
    });

    return app;
  }
};
