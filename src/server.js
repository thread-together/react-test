process.on('uncaughtException', e => {
  console.error(e);
  process.exit(1);
});

process.on('unhandledRejection', e => {
  console.error(e);
  process.exit(1);
});

const { initServer } = require('./index');
const server = initServer();
const { PORT = 3001 } = process.env;
server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT} ...`);
});
