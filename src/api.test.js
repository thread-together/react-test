const request = require('supertest');
const { initServer } = require('./index');

describe('Favorite items API', () => {
  let server;
  let lastFavoriteId;

  beforeAll(async () => {
    server = initServer();
  });

  it('gets a single item by id', async () => {
    const response = await request(server)
      .get(`/api/items/1`);

    expect(response.status).toBe(200);
    expect(response.body.item_id).toEqual(1);
  });

  xit('favorites an item', async () => {
  });

  xit('favoriting an already favorited item does not cause an error', async () => {
  });

  xit('unfavorites an item', async () => {
  });

  xit('get item returns not found if item id does not exist', async () => {
  });

  xit('favorite item returns bad request if item id does not exist', async () => {
  });

  xit('returns all items, denoting which ones are favorites', async () => {
  });
});

