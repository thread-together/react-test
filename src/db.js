// This is a not a real database. We do this to keep the
// test simple.

const products = [
  {
    id: 1,
    name: 'Product',
    price: '$52.00',
    image: '',
    variants: [
      {
        size: 'S',
        color: 'Blue',
      },
      {
        size: 'M',
        color: 'Blue',
      },
    ],
  }
];

function getAllProducts() {
  return products;
}

function getProduct(id) {
  return products.find((product) => product.id === id);
}

module.exports = { getAllProducts, getProduct };
